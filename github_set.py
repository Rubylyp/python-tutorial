import re
import requests
from bs4 import BeautifulSoup

websites = ["github.com",
            "github.global.ssl.fastly.net",
            "assets-cdn.github.com"
            ]

# 执行所有的HTML页面的解析
all_datas = []
htmls = []
for website in websites:
    url = f"https://ipaddress.com/website/%s" % website  # 页面链接
    print("craw html:", url)
    r = requests.get(url)
    soup = BeautifulSoup(r.text, 'html.parser')
    ip_info = (
        soup.find_all("tbody", id="dnsinfo")
    )
    info = ip_info[0].find_all("a")
    for i in info:
        ip = i.get_text()
        if re.search(r'[a-z]', ip) != None:
            break
        all_datas.append(ip+" " + website)

print(all_datas)

with open("hosts.txt", "w") as fout:
    for data in all_datas:
        fout.writelines(data + "\n")
